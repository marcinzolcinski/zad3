---
author: Marcin Żółciński
title: Przykładowa prezentacja
subtitle: Koszykówka
date: 06.11.2021
theme: Warsaw
output: beamer_presentation
header-includes: 
    \usepackage{xcolor}
    \usepackage{listings}
---



## Co to koszykówka?

Dyscyplina sportu drużynowego, w której dwie pięcioosobowe drużyny grają  przeciwko sobie próbując zdobyć punkty umieszczając piłkę w koszu. Jest  jedną z najpopularniejszych i najchętniej oglądanych dyscyplin na  świecie. Za datę powstania koszykówki uznaje się 21 grudnia 1891 roku, a za jej twórcę - **Jamesa Naismitha**.

![Luka Doncic](luka.jpg "Luka Doncic i Kawhi Leonard"){height=50% width=50%}


## Historia koszykówki

Powstała 21 grudnia 1891 roku w Springfield w stanie Massachusetts, gdy  nauczyciel wychowania fizycznego w YMCA James Naismith opracował grę  zespołową, którą mogliby uprawiać studenci college'u zimą w sali.  Początkowo do gry w koszykówkę używano zwykłej piłki futbolowej. Zasady  koszykówki były bardzo proste i zostały spisane przez twórcę tego  sportu. Gra ta stawała się coraz bardziej znana, a jej zasady zmieniały  się. Wkrótce koszykówka była znana w całej Ameryce Północnej. 6 czerwca  1946 zostało założone BAA (Basketball Association of America), które  jesienią 1949 przybrało nazwę National Basketball Association.

![Historyczny kort](kort.jpeg){height=50% width=50%}

## FIBA i NBA

Istnieją dwie główne odmiany gry: amatorsko-zawodowa, nadzorowana przez  Międzynarodową Federację Koszykówki **(FIBA)**, oraz w pełni profesjonalna,  będąca podstawą amerykańsko-kanadyjskiej **NBA**. Do najważniejszych różnic  pomiędzy obydwoma wariantami należą rozmiary boiska, długość czasu gry  oraz odległość linii trzech punktów od kosza. Specyfika i cel gry są  jednak takie same, co nie stwarza zawodnikom problemów z adaptacją do  którejś z odmian. Od czasu znacznego wzrostu popularności **NBA** w Europie i na świecie (lata 90. XX wieku) FIBA przejęła od niej wiele przepisów  mających na celu światowe ujednolicenie sportu. Własne przepisy  posiadają także rozgrywki *NCAA* i *WNBA*.

## Drużyny

W koszykówkę grają dwie drużyny, po pięciu zawodników każda. Celem  każdej z drużyn jest zdobywanie punktów za celne rzuty do kosza  przeciwnika i zapobieganie zdobywaniu punktów przez drużynę przeciwną.  Nad właściwym przebiegiem gry czuwają sędziowie, sędziowie stolikowi i  komisarz. Zwycięzcą meczu zostaje drużyna, która na koniec czasu gry  uzyska większą liczbę punktów.
![druzyny](druzyny.jpg){height=50% width=50%}

## Boisko

Kosze umieszczone są na wysokości 3,05 metra . Punkty otrzymuje się za umieszczenie piłki  w koszu:

- 1 punkt za udany rzut osobisty z linii rzutów wolnych
- 2 punkty za rzut z akcji wykonany z odległości mniejszej niż linia rzutów za trzy punkty
- 3 punkty za rzut zza linii rzutów za trzy punkty (6,75m) [w NBA 7,24 m]

Boisko do gry to płaska, twarda powierzchnia wolna od przeszkód, o wymiarach  28m długości i 15m szerokości, mierzonych od wewnętrznych krawędzi linii ograniczających boisko.

![kort](nowy_kort.jpeg){height=50% width=50%}